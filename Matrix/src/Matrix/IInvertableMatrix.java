package Matrix;

public interface IInvertableMatrix extends IMatrix {
    Matrix getInverseMatrix();
}