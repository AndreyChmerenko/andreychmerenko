import org.junit.Test;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LambdaRunnerTest {
    private Human pasha = new Human("Паша", "Петрович", "Петрек", 19,Gender.MALE);
    private Human dima = new Human("Дима", "Андреевич", "Петрек", 18,Gender.MALE);
    private Human dasha = new Human("Даша", "Сергеевна", "Иванова", 18,Gender.FEMALE);

    @Test
    public void stringGetLengthTest(){
        assertEquals(9, LamdaRunner.stringGetLength("qazwsxedc"));
    }

    @Test
    public void stringGetFirstSymbolTest(){
        assertEquals('q', LamdaRunner.stringGetFirstSymbol("qazwsxedc"));
    }

    @Test
    public void stringHaveNoSpaceTest(){
        assertTrue(LamdaRunner.stringHaveNoSpace("qazwsxedc"));
        assertFalse(LamdaRunner.stringHaveNoSpace("qazws xedc"));
    }

    @Test
    public void getNumberOfWordsTest(){
        assertEquals(3, LamdaRunner.getNumberOfWords("qaz,wsx,edc"));
        assertEquals(1, LamdaRunner.getNumberOfWords("qazwsxedc"));
    }

    @Test
    public void  getHumanAgeTest(){
        assertEquals(19, LamdaRunner.getHumanAge(pasha));
    }

    @Test
    public void namesakesTest(){
        assertTrue(LamdaRunner.namesakes(dima, pasha));
    }

    @Test
    public void getFullNameTest(){
        assertEquals("Петрек Паша Петрович", LamdaRunner.getFullName(pasha));
    }

    @Test
    public void happyBirthdayTest(){
        assertEquals(20, LamdaRunner.happyBirthday(pasha).getAge());
    }

    @Test
    public void humanYoungerMaxAgeTest(){
        assertTrue( LamdaRunner.humanYoungerMaxAge(pasha, dima, dasha, 10));
        assertFalse( LamdaRunner.humanYoungerMaxAge(pasha, dima, dasha,117));
    }

}
