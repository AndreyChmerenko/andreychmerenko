import org.junit.Test;
import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class StreamApiDemoTest {

    @Test
    public void removeNullValueTest(){
        List list = new ArrayList<>();
        Collections.addAll(list,
                true, 15, null, "ok", null);

        List result = new ArrayList<>();
        Collections.addAll(result,
                true,15,"ok");

        assertEquals(result, StreamApiDemo.removeNullValue.apply(list));
    }

    @Test
    public void countPositiveNumberTest(){
        Set<Integer> s = new HashSet<>(5);
        Collections.addAll(s,
                3, 0, 4, 6, -4);
        assertEquals(3, (long)StreamApiDemo.countPositiveNumber.apply(s));
    }

    @Test
    public void returnThreeElemsTest(){
        List list = new ArrayList<>();
        Collections.addAll(list,
                true, 15, null, "ok", null);
        List result = new ArrayList<>();
        Collections.addAll(result,
                null,"ok",null);
        assertEquals(result,StreamApiDemo.returnThreeElems.apply(list));
    }

    @Test
    public void getFirstOddValueTest(){
        List<Integer> listOfInt = new ArrayList<>();
        Collections.addAll(listOfInt,
                1, 3, 5, 7, 21, 11, 99, 981, 13, 441, 421);
        assertEquals(null,StreamApiDemo.getFirstOddValue.apply(listOfInt));

        List<Integer> listOfInt1 = new ArrayList<>();
        Collections.addAll(listOfInt1,
                1, 2, 5, 4, 21, 5, 99, 98, 13, 44, 42);
        assertEquals(2,(int)StreamApiDemo.getFirstOddValue.apply(listOfInt1));
    }

    @Test
    public void squaresOfElemsTest(){
        Integer[] arr = {1,4,4,25,26,36};
        List<Integer> result = new ArrayList<>();
        Collections.addAll(result,
                1,16,625,676,1296);
        List<Integer> actual = new ArrayList<>();
        actual = StreamApiDemo.squaresOfElems.apply(arr);
        assertEquals(result,actual);
    }

    @Test
    public void  nonEmptyStringsTest(){
        List<String> listOfString = new ArrayList<>();
        Collections.addAll(listOfString,
                "dima", "", null, "42", "glokaya kuzdra budlanula bokra i kudryachit bokrenka", "Дима");
        List<String> result = new ArrayList<>();
        Collections.addAll(result,
                 "42", "dima","glokaya kuzdra budlanula bokra i kudryachit bokrenka","Дима");
        assertEquals(result,StreamApiDemo.nonEmptyStrings
                .apply(listOfString));
    }

    @Test
    public void setOfStringTurnInListTest(){
        Set<String> setOfString = new HashSet<>();
        Collections.addAll(setOfString,
                "dima", "", "42", "glokaya kuzdra budlanula bokra i kudryachit bokrenka", "Дима");
        List<String> result = new ArrayList<>();
        Collections.addAll(result,
                "Дима","glokaya kuzdra budlanula bokra i kudryachit bokrenka","dima","42","");
        assertEquals(result,StreamApiDemo.setOfStringTurnInList.apply(setOfString));
    }

    @Test
    public void summarySquaresTest(){
        Set<Integer> s = new HashSet<>(5);
        Collections.addAll(s,
                3, 0, 4, 6, -4);
        assertEquals(77,(int)StreamApiDemo.summarySquares.apply(s));
    }

    @Test
    public void calculateMaxAgeTest(){
        Collection<Human> colOfHuman = new ArrayList<>();
        Collections.addAll(colOfHuman,
                new Human("Паша", "Петрович", "Петрек", 20, Gender.MALE),
                new Human("Дима", "Андреевич", "Петрек", 19, Gender.MALE),
                new Human("Даша", "Сергеевна", "Иванова", 18, Gender.FEMALE),
                new Human("Саша","Петров","Владимирович", 200, Gender.MALE));
        assertEquals(200,(int)StreamApiDemo.calculateMaxAge.apply(colOfHuman));
    }

    @Test
    public void sortByGenderSortByAgeTest(){
        Collection<Human> colOfHuman = new ArrayList<>();
        Collections.addAll(colOfHuman,
                new Human("Паша", "Петрович", "Петрек", 20, Gender.MALE),
                new Human("Дима", "Андреевич", "Петрек", 19, Gender.MALE),
                new Human("Даша", "Сергеевна", "Иванова", 18, Gender.FEMALE),
                new Human("Саша","Петров","Владимирович", 20, Gender.MALE),
                new Human("Татьяна", "Андреевна", "Фокина", 23, Gender.FEMALE),
                new Human("Даниил", "Крестобог", "Поляков", 21, Gender.MALE),
                new Human("Дмитрий", "Справа", "Ткаченко", 42, Gender.MALE),
                new Human("Полина", "Анатольевна", "Козлова", 22, Gender.FEMALE),
                new Human("Александра", "C", "Гридина", 14, Gender.FEMALE));
        Collection<Human> result = new ArrayList<>();
        Collections.addAll(result,
                new Human("Дмитрий", "Справа", "Ткаченко", 42, Gender.MALE),
                new Human("Даниил", "Крестобог", "Поляков", 21, Gender.MALE),
                new Human("Паша", "Петрович", "Петрек", 20, Gender.MALE),
                new Human("Саша","Петров","Владимирович", 20, Gender.MALE),
                new Human("Дима", "Андреевич", "Петрек", 19, Gender.MALE),
                new Human("Татьяна", "Андреевна", "Фокина", 23, Gender.FEMALE),
                new Human("Полина", "Анатольевна", "Козлова", 22, Gender.FEMALE),
                new Human("Даша", "Сергеевна", "Иванова", 18, Gender.FEMALE),
                new Human("Александра", "C", "Гридина", 14, Gender.FEMALE));
        assertEquals(result,StreamApiDemo.sortByGenderSortByAge.apply(colOfHuman));
    }
}
