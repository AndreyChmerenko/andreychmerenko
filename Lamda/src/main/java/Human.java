public class Human {
    private String firstName, middleName, lastName;
    private int age;
    Gender gender;

    public Human(){}

    public Human(String firstName, String middleName, String lastName, int age, Gender gender) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
    }

    public Gender getGender(){
        return this.gender;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (getAge() != human.getAge()) return false;
        if (!getFirstName().equals(human.getFirstName())) return false;
        if (!getMiddleName().equals(human.getMiddleName())) return false;
        return getLastName().equals(human.getLastName());
    }

    @Override
    public int hashCode() {
        int result = getFirstName().hashCode();
        result = 31 * result + getMiddleName().hashCode();
        result = 31 * result + getLastName().hashCode();
        result = 31 * result + getAge();
        return result;
    }

    @Override
    public String toString(){
        return getLastName() + " " + getFirstName() + " " + getMiddleName();
    }

    public int compareToAge(Human other){
        if(this.getAge() == other.getAge()) return 0;
        return this.getAge() > other.getAge() ? -1 : 1;
    }

    public int compareToGender(Human other){
        return this.getGender().compareTo(other.getGender());
    }
}

