import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamApiDemo extends LambdaDemo{
    public static Function<List<?>, List<?>> removeNullValue = (list) ->
            (list.stream().filter(x -> x != null )).collect(Collectors.toList());
    public static Function<Set<Integer>, Long> countPositiveNumber = (set) ->
            set.stream().filter(x -> x > 0).count();
    public static Function<List<?>, List<?>> returnThreeElems = (list) ->
            list.stream().skip(list.size() - 3).collect(Collectors.toList());
    public static Function<List<Integer>, Integer> getFirstOddValue =
            (list) -> list.stream().filter(x -> x % 2 == 0).findFirst().orElse(null);
    public static Function<Integer[], List<Integer>> squaresOfElems =
            (arr) -> Arrays.stream(arr).distinct().map(x -> x*x).collect(Collectors.toList());
    public static Function<List<String>, List<String>> nonEmptyStrings =
            (list) -> list.stream().filter(x -> x != null && !(x.equals(""))).sorted().collect(Collectors.toList());
    public static Function<Set<String>, List<String>> setOfStringTurnInList =
            (set) -> set.stream().sorted((x, y) -> -x.compareTo(y)).collect(Collectors.toList());
    public static Function<Set<Integer>, Integer> summarySquares =
            (set) -> set.stream().mapToInt(x -> x*x).sum();
    public static Function<Collection<Human>, Integer> calculateMaxAge =
            (col) -> col.stream().sorted((x, y) -> x.compareToAge(y)).findFirst().get().getAge();
    public static Function<Collection<Human>, Collection<Human>> sortByGenderSortByAge =
            (col) -> col.stream().sorted((x,y) -> x.compareToAge(y)).sorted((x,y)->
                    x.compareToGender(y)).collect(Collectors.toList());
}