public class LamdaRunner {
    public static int stringGetLength(String str){
        return LambdaDemo.stringGetLength.apply(str);
    }

    public static char stringGetFirstSymbol(String str){
        return LambdaDemo.stringGetFirstSymbol.apply(str);
    }

    public static boolean stringHaveNoSpace(String str) {
        return LambdaDemo.stringHaveNoSpace.test(str);
    }

    public static int getNumberOfWords(String str){
        return LambdaDemo.getWordsNumbers.apply(str);
    }

    public static int getHumanAge(Human hum){
        return LambdaDemo.getHumanAge.apply(hum);
    }

    public static boolean namesakes(Human hum1, Human hum2){
        return  LambdaDemo.namesakes.test(hum1, hum2);
    }

    public static String getFullName(Human hum){
        return LambdaDemo.getFullName.apply(hum);
    }

    public static Human happyBirthday(Human hum){
        return LambdaDemo.happyBirthday.apply(hum);
    }

    public static boolean humanYoungerMaxAge(Human hum1, Human hum2, Human hum3, int age){
        return LambdaDemo.humanYoungerMaxAge.test(hum1, hum2, hum3, age);
    }
}
