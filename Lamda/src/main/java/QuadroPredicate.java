@FunctionalInterface
public interface QuadroPredicate<A, B, C, D> {
    boolean test(A a, B b, C c, D d);
}
