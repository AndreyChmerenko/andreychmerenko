import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaDemo {
    public static final Function<String, Integer> stringGetLength = str -> str!=null ? str.length() : null;
    public static final Function<String, Character> stringGetFirstSymbol = str -> str!=null ? str.charAt(0) : null;
    public static final Predicate<String> stringHaveNoSpace = str -> str!=null ? !str.contains(" ") : null;
    public static final Function<String, Integer> getWordsNumbers = str -> str!= null ? str.split(",").length : null;
    public static final Function<Human, Integer> getHumanAge = Human::getAge;
    public static final BiPredicate<Human, Human> namesakes = (hum1, hum2) ->
            hum1.getLastName().equals(hum2.getLastName());
    public static final Function<Human, String> getFullName = Human::toString;
    public static final Function<Human, Human> happyBirthday = (hum) ->
            new Human(hum.getFirstName(), hum.getMiddleName(), hum.getLastName(), hum.getAge()+1,hum.getGender());
    public static final QuadroPredicate<Human, Human, Human, Integer> humanYoungerMaxAge =
            (hum1, hum2, hum3, age) -> hum1.getAge() > age && hum2.getAge() > age && hum3.getAge() > age;
}
